from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.utils import timezone

APPLYER_LIST = ((0, "新郎様"), (1, "新婦様"), (2, "その他"))
BRIDAL_PLACE_LOST = (
    (0, "アルカンシエル金沢"), (1, "ル・センティフォーリア大阪"), (2, "アイネスヴィラノッツェ宝ヶ池"))
CONTACT_TIME_LIST = ((0, "~12:00"), (1, "~17:00"),
                     (2, "17:00~"), (3, "何時でも構わない"))


class Post(models.Model):
    class Meta:
        verbose_name = 'フォームデータ'
        verbose_name_plural = 'フォームデータ'

    id = models.CharField('フォームID', max_length=6, primary_key=True)
    place = models.PositiveSmallIntegerField(
        'ご希望会場', choices=BRIDAL_PLACE_LOST)
    husband_name = models.CharField('新郎様お名前', max_length=30)
    wife_name = models.CharField('新婦様お名前', max_length=30)
    applyer_name = models.CharField('申込者様お名前', max_length=30)
    is_married = models.BooleanField('入籍状況', null=True)
    is_party = models.BooleanField('披露宴開催有無', null=True)
    email_address = models.EmailField('emailAdress', unique=True)
    tel_num = models.CharField('電話番号', max_length=13)
    prefered_date = models.DateField('ご希望のお日にち', null=True)
    prefered_time = models.TimeField('ご希望のお時間', null=True)
    better_hours = models.PositiveSmallIntegerField(
        '連絡の取りやすい時間帯', choices=CONTACT_TIME_LIST)
    apply_reason = models.CharField('応募動機', max_length=300, null=True)
    applied_date = models.DateTimeField(
        '応募日時', blank=True, null=True, default=timezone.now)

    def __str__(self):
        return '来店予約：' + ' ' + self.applyer_name + \
            ' ' + str(self.prefered_date) + ' ' + str(self.prefered_time)
        # return '来店予約：' + ' ' + self.husband_name + ' ' + self.wife_name + \
        #     ' ' + str(self.prefered_date.strftime("%Y%m%d-%H:%M:%S"))
