from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from .models import Post
from marry_lp import settings
from django.http import HttpResponse
from django.core.mail import BadHeaderError, send_mail
from django.template.loader import render_to_string
from django.contrib import messages
from django.core.exceptions import ValidationError


TEL_LIST = ['076-210-7227', '076-255-1356', '076-255-1356']
BRIDAL_PLACE_LIST = ['アルカンシエル金沢', 'ル・センティフォーリア大阪', 'アイネスヴィラノッツェ宝ヶ池']


class DateInput(forms.DateInput):
    input_type = 'date'


class TimeInput(forms.DateInput):
    input_type = 'time'


class PostForm(forms.ModelForm):

    class Meta:
        widgets = {'prefered_date': DateInput(), 'prefered_time': TimeInput()}
        model = Post
        exclude = (
        )
        fields = (
            'place',
            'husband_name',
            'wife_name',
            'applyer_name',
            'is_married',
            'is_party',
            'email_address',
            'tel_num',
            'prefered_date',
            'prefered_time',
            'better_hours',
            'apply_reason',
            'applied_date')
        labels = {
            'place': 'ご希望会場',
            'husband_name': '新郎様お名前',
            'wife_name': '新郎様お名前',
            'applyer_name': 'お申し込み者様',
            'is_married': '入籍状況',
            'is_party': '披露宴（お食事会）の有無',
            'email_address': 'メールアドレス',
            'tel_num': 'お申込者様電話番号',
            'prefered_date': '来館希望日',
            'prefered_time': '来館希望時間',
            'better_hours': '連絡の取りやすい時間帯',
            'apply_reason': '応募動機',
            'applied_date': '応募日時'
        }

# 以下メールアドレス確認ーーーーーーーー
    # def clean_verify_email(self):
    #     email = self.cleaned_data.get('email_address')
    #     verify_email = self.cleaned_data['verify_email']
    #     if email != verify_email:
    #         print("error---------" +
    #               str(forms.ValidationError('not same email')))
    #         raise forms.ValidationError(
    #             '入力されたEmailが一致しません。'.format(email, verify_email))
        # return verify_email

# 以下その他バリデーション処理メモーーーーーーーー
    # def clean(self):
    #     # all_clean_data = super().clean()
    #     all_clean_data = self.cleaned_data
    #     email = all_clean_data.get('email_address')
    #     if "a" in email:
    #         raise forms.ValidationError("do not   use a")
        # verify_email = self.cleaned_data['verify_email']
        # verify_email = all_clean_data.get('verify_email')

        # if email != verify_email:
        #     msg = "make sure email is same"
        #     self.add_error('verify_email', msg)
        #     raise forms.ValidationError(msg)
        # return all_clean_data

# 以下メール送信（BCCに式場のアドレスを登録しておくßことも可能→Emailクラスを使う）

    def send_email(self):
        subject = "confirm"
        place = BRIDAL_PLACE_LIST[self.cleaned_data['place']]
        husband_name = self.cleaned_data['husband_name']
        wife_name = self.cleaned_data['wife_name']
        applyer_name = self.cleaned_data['applyer_name']
        is_married = self.cleaned_data['is_married']
        is_party = self.cleaned_data['is_party']
        email = self.cleaned_data['email_address']
        tel_num = self.cleaned_data['tel_num']
        prefered_date = self.cleaned_data['prefered_date']
        prefered_time = self.cleaned_data['prefered_time']
        better_hours = self.cleaned_data['better_hours']
        apply_reason = self.cleaned_data['apply_reason']
        place_tel = TEL_LIST[self.cleaned_data['place']]

        context = {
            "place": place,
            "husband_name": husband_name,
            "wife_name": wife_name,
            "applyer_name": applyer_name,
            "is_married": is_married,
            "is_party": is_party,
            "email": email,
            "tel_num": tel_num,
            "prefered_date": prefered_date,
            "prefered_time": prefered_time,
            "better_hours": better_hours,
            "apply_reason": apply_reason,
            "place_tel": place_tel}

        message = render_to_string(
            'mails/mail.txt',
            context)  # request can be assigned as next parameter
        from_email = settings.EMAIL_HOST_USER
        recipient_list = [
            email, ]  # 受信者リスト
        bcc = [settings.EMAIL_HOST_USER, settings.EMAIL_ADMIN]
        print("-----email will be sent soon")
        try:
            send_mail(subject, message, from_email, recipient_list, bcc)
        except BadHeaderError:
            return HttpResponse("無効なヘッダが検出されました。")

    def save(self, commit=True):
        if not commit:
            raise NotImplementedError('form error')
        try:
            max_id = Post.objects.latest('id').id
        except ObjectDoesNotExist:
            max_id = 'P000'
        post_id = 'P' + (str(int(max_id[1:]) + 1).zfill(3))
        place = self.cleaned_data['place']
        husband_name = self.cleaned_data['husband_name']
        wife_name = self.cleaned_data['wife_name']
        applyer_name = self.cleaned_data['applyer_name']
        is_married = self.cleaned_data['is_married']
        is_party = self.cleaned_data['is_party']
        email_address = self.cleaned_data['email_address']
        tel_num = self.cleaned_data['tel_num']
        prefered_date = self.cleaned_data['prefered_date']
        prefered_time = self.cleaned_data['prefered_time']
        better_hours = self.cleaned_data['better_hours']
        apply_reason = self.cleaned_data['apply_reason']
        applied_date = timezone.datetime.now()

        post = Post(
            id=post_id,
            place=place,
            husband_name=husband_name,
            wife_name=wife_name,
            applyer_name=applyer_name,
            is_married=is_married,
            is_party=is_party,
            email_address=email_address,
            tel_num=tel_num,
            prefered_date=prefered_date,
            prefered_time=prefered_time,
            better_hours=better_hours,
            apply_reason=apply_reason,
            applied_date=applied_date)
        post.save()
        return post
