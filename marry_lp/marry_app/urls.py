from django.urls import path
from .views import ManageForm
# from .views import apply_form, done_view
# from .views import ApplyForm, DoneView, Sample
from marry_lp import urls

app_name = 'campaign'
urlpatterns = [
    path('', ManageForm.manage_form, name='apply_form'),
    path('done/', ManageForm.show_done_view, name='form_done'),
]
