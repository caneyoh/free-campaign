from django.shortcuts import render, redirect
from .forms import PostForm
from django.contrib import messages
from .prohibitDoubleSubmit import set_submit_token, exists_submit_token


class ManageForm:

    def manage_form(request):
        if request.method == "POST":
            if not exists_submit_token(request):
                return render(request, 'marry_app/form_error.html', {})
            else:
                form = PostForm(request.POST)
                if not form.is_valid():
                    msg = '無効な入力がされました。再度やり直してください'
                    messages.error(request, msg)
                    # return redirect('campaign:apply_form')
                    # return render(request, 'index.html',
                    #               {'form': form})
                    anchor = 'form'
                    return render(request, 'marry_app/form.html',
                                  {'form': form, 'anchor': anchor})
                form.save(commit=True)

                print("---saved---")
                form.send_email()
                # return render(request,
                #               'form_done.html',
                #               {form: form})
                return render(request,
                              'marry_app/form_done.html',
                              {form: form})
        else:
            submit_token = set_submit_token(request)
            form = PostForm()
            # return render(request, 'index.html',
            #               {'form': form, "submit_token": submit_token})
            return render(request, 'marry_app/form.html',
                          {'form': form, "submit_token": submit_token})

    def show_done_view(request):
        print("form ---- done ")
        # return render(request, 'form_done.html')
        return render(request, 'marry_app/form_done.html')


# 以下別のフォーム方法ーーーーーーーー
# class ApplyForm(CreateView):
#     model = Post
#     template_name = 'marry_app/form.html'
#     form_class = PostForm
#     # success_url = reverse_lazy('form_done')
#     success_url = '/'


# apply_form = ApplyForm.as_view()


# class DoneView(TemplateView):
#     template_name = 'marry_app/form_done.html'
#     print("done---------------")


# done_view = DoneView.as_view()
