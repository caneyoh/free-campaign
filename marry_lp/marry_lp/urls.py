"""marry_lp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import include, path
from . import settings
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from marry_app.views import ManageForm


def render_index(request):
    return render(request, 'index.html')


def render_a(request):
    return HttpResponse('aaa')


urlpatterns = [
    path('ykhb-admin/', admin.site.urls),
    # path('', ManageForm.manage_form, name='apply_form'),
    path('', include('marry_app.urls', namespace='campaign')),
    # path('done/', ManageForm.show_done_view, name='form_done'),
    # path('campaign/', include('marry_app.urls', namespace='campaign')),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
