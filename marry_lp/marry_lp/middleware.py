from django.http import HttpResponsePermanentRedirect
from django.utils.deprecation import MiddlewareMixin


class RedirectHostMiddleware(MiddlewareMixin):

    def process_request(self, request):
        host = request.get_host()
        if host and ('herokuapp' in host):
            # if host and ('herokuapp' in host):
            my_domain = 'dream-wedding-cp.com'
            redirect_url = 'https://%s%s' % (
                my_domain, request.get_full_path()
            )
            return HttpResponsePermanentRedirect(redirect_url)

        if 's' not in request.schema:
            redirect_url = 'https://%s%s' % (my_domain, request.get_full_path)
            return HttpResponsePermanentRedirect(redirect_url)
